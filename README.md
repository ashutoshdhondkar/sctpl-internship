# SCTPL Internship

## project details

### 1. ExcelSort:
    
    
    Here you have to provide my django website with a csv or an excel file and 
    provide ascending or descending order for my website to sort it accordingly.
    My website will return you an excel or csv file in a downloadable format.
    The demonstration is as shown :-
    https://www.youtube.com/watch?v=ChMxc4A73eA
    
### 2. Coin Simulator
    
    This is a desktop app made from tkinter module of python. There will be a 
    welcome screen wherein you have to select your option of heads or tails and 
    specify number of trials. After that the main screen will popup where the actual 
    tossing of coin happens. There is a count maintained of number of heads and 
    tails occured and the winner is declared accordingly. Here the randomness 
    is implemented using 'random' module of python
    
### 3. Company manager

    This is purely a console based application. Here the database of each 
    category of employee is maintained. There will be three main options such as
    hire,fire,raise. 
    
### 4. quizmaker
    
    This is an application of python-django(v 2.0.6). The site is available on
    https://quizbook.pythonanywhere.com/
    
