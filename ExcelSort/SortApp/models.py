from django.db import models

# Create your models here.
class FileUploads(models.Model):
    filepath = models.FileField(upload_to='files/')
