from django.apps import AppConfig


class SortappConfig(AppConfig):
    name = 'SortApp'
